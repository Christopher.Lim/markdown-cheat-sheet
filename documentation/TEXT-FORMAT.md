# Text Format
### Bold
```
**bold**
__bold__
```
**Testing bold** \
__Testing bold__

### Italics
```
*italics*
_italics_
```
*Testing italics* \
_Testing italics_

### Strikethrough
```
~~strikethrough~~
```
~~Testing strikethrough~~

### Blockquote
```
> Blockquote
```
> Testing blockquote

### Inline Code
```
`inline code`
```
`Testing inline code`

### Code Block
```
Use three back ticks (`)
``` code block  ```
```
```javascript
Testing Code Block
console.log('Hellow World');
```
```diff
Testing Code Block
console.log('Hello World')
```